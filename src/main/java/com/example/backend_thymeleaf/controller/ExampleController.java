package com.example.backend_thymeleaf.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/examples_templates/")
public class ExampleController {

    public static final String DEFAUL_VIEW = "example";
    public static final String DEFAUL_MODEL_AND_VIEW = "exampleModelAndView";

    @GetMapping("example_string")
    public String exampleString(){
        return DEFAUL_VIEW;
    }

    @GetMapping("exampleMAV")
    public ModelAndView exampleModelAndView(){
        return new ModelAndView (DEFAUL_VIEW);
    }

    @GetMapping("return_string_model")
    public String stringAndModel(Model model){
        model.addAttribute("name","Deogracio");
        model.addAttribute("age","29");

        return DEFAUL_MODEL_AND_VIEW;
    }

    @GetMapping("return_model_and_view")
    public ModelAndView modelAndView(){
        ModelAndView modelAndView = new ModelAndView(DEFAUL_MODEL_AND_VIEW);
        modelAndView.addObject("name", "Deogracio");
        modelAndView.addObject("age", "29");

        return modelAndView;

    }

}
