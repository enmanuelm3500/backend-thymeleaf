package com.example.backend_thymeleaf.controller.handler;


import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class handlerErrorController {

    public static final String VIEW = "error/500";

    @ExceptionHandler(Exception.class)
    public String showInternalError(){
        return VIEW;
    }

}
