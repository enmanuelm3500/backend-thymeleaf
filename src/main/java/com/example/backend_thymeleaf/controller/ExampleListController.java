package com.example.backend_thymeleaf.controller;


import com.example.backend_thymeleaf.model.Person;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/example_list")
public class ExampleListController {

    @GetMapping("/return_string_model")
    public String returnModelComplex(Model model){
        model.addAttribute("people", getPeople());
        return "exampleListMode";
    }

    @GetMapping("/return_model_and_view")
    public ModelAndView returnModelAndViewComplexModel(){
        ModelAndView modelAndView = new ModelAndView("exampleListMode");
        modelAndView.addObject("people",getPeople());
        return modelAndView;
    }

    private List<Person> getPeople(){
        ArrayList<Person> personArrayList = new ArrayList<>();
        personArrayList.add(new Person("Deogracio", 29));
        personArrayList.add(new Person("Maria", 45));
        personArrayList.add(new Person("Juan Manuel", 1));
        return personArrayList;
    }

}
