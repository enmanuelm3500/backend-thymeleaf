package com.example.backend_thymeleaf.controller;


import com.example.backend_thymeleaf.entity.Course;
import com.example.backend_thymeleaf.model.CourseModel;
import com.example.backend_thymeleaf.service.CourseService;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@RestController
@RequestMapping("/course")
public class CourseController {


    private static final Log LOG = LogFactory.getLog(CourseController.class);

    @Autowired
    private CourseService courseService;

    @GetMapping("/list")
    public ModelAndView listAll(){
        LOG.info("Call METHOD:"+"listAll()");
        ModelAndView modelAndView = new ModelAndView("courses");
        modelAndView.addObject("courses",courseService.listAll());
        modelAndView.addObject("course",new CourseModel());
        return modelAndView;
    }

    @GetMapping("/")
    public RedirectView redirect(){
        return new RedirectView("/course/list");
    }

    @PostMapping("/add")
    public RedirectView addCourse(@ModelAttribute("course") CourseModel course){
        LOG.info("Call METHOD:"+"---addCourse()"+"---Params:"+course.toString());
        courseService.addCourse(course);
        return new RedirectView("/course/list");
    }



}
