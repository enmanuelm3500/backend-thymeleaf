package com.example.backend_thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendThymeleafApplication.class, args);
	}

}
