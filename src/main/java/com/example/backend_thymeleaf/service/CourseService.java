package com.example.backend_thymeleaf.service;

import com.example.backend_thymeleaf.entity.Course;
import com.example.backend_thymeleaf.model.CourseModel;

import java.util.List;

public interface CourseService {

    List<CourseModel> listAll();

    Course addCourse(CourseModel course);

    int delete(int id);

    Course update(Course course);
}
